type Circle = {
  x: number
  y: number
  r: number
}

export default class CirclePacker {
  constructor(width:number, height:number, numGrid?:number, padding?:number);
  addCircle(c:Circle):Circle;
  tryToAddCircle(x:number, y:number, minRadius?:number, maxRadius?:number, actuallyAdd?:boolean):Circle;
  tryToAddShape(circles:Circle[], actuallyAdd?:boolean):Circle[];
  removeCircles(x:number, y:number, radius:number):boolean;
}